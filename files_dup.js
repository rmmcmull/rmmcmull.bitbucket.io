var files_dup =
[
    [ "EncoderDriver.py", "EncoderDriver_8py.html", [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "lab4.py", "lab4_8py.html", "lab4_8py" ],
    [ "LAB_0X01.py", "LAB__0X01_8py.html", "LAB__0X01_8py" ],
    [ "LAB_0X02.py", "LAB__0X02_8py.html", "LAB__0X02_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_backend.py", "main__backend_8py.html", "main__backend_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "MotorDriver.py", "MotorDriver_8py.html", [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "temperature_plotter.py", "temperature__plotter_8py.html", "temperature__plotter_8py" ],
    [ "TouchPanelDriver.py", "TouchPanelDriver_8py.html", "TouchPanelDriver_8py" ],
    [ "UI_front.py", "UI__front_8py.html", "UI__front_8py" ]
];