var classTouchPanelDriver_1_1touchy =
[
    [ "__init__", "classTouchPanelDriver_1_1touchy.html#aa1f3dddc477bc9a91ba4de669dcfc4ae", null ],
    [ "read_data", "classTouchPanelDriver_1_1touchy.html#a46db89c904f2a54d368569d48fd323e2", null ],
    [ "x_read", "classTouchPanelDriver_1_1touchy.html#a5da73976226499f72bf495f9c51d4182", null ],
    [ "y_read", "classTouchPanelDriver_1_1touchy.html#a27cff4f6c67172615c2c39b21dfe9729", null ],
    [ "z_read", "classTouchPanelDriver_1_1touchy.html#a4bb4dfac63c20b0d241393c2cb8eafa4", null ],
    [ "x_cent", "classTouchPanelDriver_1_1touchy.html#ac8f22d68d0ff82fbac5879d21c8389b0", null ],
    [ "x_m", "classTouchPanelDriver_1_1touchy.html#a11c35a6c227003a4828b28d34c293a12", null ],
    [ "x_p", "classTouchPanelDriver_1_1touchy.html#ae56af92dc9bc11f249a034ca81887284", null ],
    [ "x_tot", "classTouchPanelDriver_1_1touchy.html#afa6dd086d955488ab37876882f89b8f5", null ],
    [ "y_cent", "classTouchPanelDriver_1_1touchy.html#a707135819996a5138191756f06222525", null ],
    [ "y_m", "classTouchPanelDriver_1_1touchy.html#a824a7857e757d3628146bb95ae6bea26", null ],
    [ "y_p", "classTouchPanelDriver_1_1touchy.html#af5055b63db7129644accd159a3aed5c8", null ],
    [ "y_tot", "classTouchPanelDriver_1_1touchy.html#ab6951cc0825e6007929cd33fa6c50d5f", null ]
];