var UI__front_8py =
[
    [ "plotData", "UI__front_8py.html#ae27d5455e6fde87135ecd76571716580", null ],
    [ "sendChar", "UI__front_8py.html#a5c7fd7dbcecf709585fb5262bf1eb27d", null ],
    [ "change", "UI__front_8py.html#a38d9193112baeb13e0b3c5ee58858773", null ],
    [ "f", "UI__front_8py.html#ac48180a44f2a768bd74f3bc1487db1cb", null ],
    [ "inv", "UI__front_8py.html#a71de00519b7e0a4af899c1cc96afc50d", null ],
    [ "n", "UI__front_8py.html#abf21c887e455a882cff1abe34d4c962f", null ],
    [ "ser", "UI__front_8py.html#a26d65cb7ea7d47e295007598eedfb6ba", null ],
    [ "state", "UI__front_8py.html#aba1a1cd5eb431e9b64c8a3cee1fdfecc", null ],
    [ "time", "UI__front_8py.html#a05b51f39b3c125dd23e1bbdec1788e7b", null ],
    [ "tup1", "UI__front_8py.html#a633ea1c78f639cf3a2c3cb70063f30bb", null ],
    [ "value", "UI__front_8py.html#ad2d4bbde3cf3e5e563a51491a8de0e08", null ],
    [ "values", "UI__front_8py.html#a4c3294fe23effef9d0b906183382de14", null ],
    [ "volt", "UI__front_8py.html#acab32a7cc46456da4a4efb838dfc2515", null ],
    [ "volts", "UI__front_8py.html#a65abc62bfa94cbdc3d101537ed952e35", null ],
    [ "writer", "UI__front_8py.html#a456a58e358a49e4d5a213c00bbfe6a66", null ]
];