var searchData=
[
  ['adc_1',['adc',['../main__backend_8py.html#a4dcbd9670b2d9535a1a6861ba1bb9f4c',1,'main_backend']]],
  ['adcall_2',['adcall',['../lab4_8py.html#a43ba7b09e4e30978811aed06525e3063',1,'lab4']]],
  ['address_3',['address',['../classmcp9808_1_1mcp9808.html#a2f44cead8ac0fa0587e52a7e71bb5019',1,'mcp9808::mcp9808']]],
  ['amb_5ftemp_4',['amb_temp',['../lab4_8py.html#acac0eeb8bee22456ed6bc24ea04a7d99',1,'lab4']]],
  ['amb_5ftemps_5',['amb_temps',['../temperature__plotter_8py.html#a3ebfd25c7a7303d692f2492a3fc6d6ac',1,'temperature_plotter']]],
  ['amount_6',['amount',['../LAB__0X01_8py.html#ac0a87c361097e26451b520ae6ec8fa0f',1,'LAB_0X01']]],
  ['avg_5ftime_7',['avg_time',['../LAB__0X02_8py.html#a364931fb795f1009c45ed49fc070fbe2',1,'LAB_0X02']]],
  ['avgtimefct_8',['avgTimeFCT',['../LAB__0X02_8py.html#a71d8e9f6e2de5a9a094cc90fa3f6f8b0',1,'LAB_0X02']]]
];
