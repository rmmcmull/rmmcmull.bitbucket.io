var classLAB__0X07_1_1touchy =
[
    [ "__init__", "classLAB__0X07_1_1touchy.html#a1210e559a52b9db409691ebe11e5da2e", null ],
    [ "read_data", "classLAB__0X07_1_1touchy.html#a650f9b67fcb0ec0b780cfb6876d118f9", null ],
    [ "x_read", "classLAB__0X07_1_1touchy.html#a0181567e1aa8518d3925a8f544f9000b", null ],
    [ "y_read", "classLAB__0X07_1_1touchy.html#a9bcd1f761946dd7bccf174d621940a0c", null ],
    [ "z_read", "classLAB__0X07_1_1touchy.html#aa94619afbac3ffe1302eb8f207a3ccad", null ],
    [ "x_cent", "classLAB__0X07_1_1touchy.html#a38f0cabbd7fcdeea92de55edb9ca03c1", null ],
    [ "x_m", "classLAB__0X07_1_1touchy.html#a27e0f515a95a8e75661c539df24eca33", null ],
    [ "x_p", "classLAB__0X07_1_1touchy.html#abd4290262a581db9db5d05ebde3775e0", null ],
    [ "x_tot", "classLAB__0X07_1_1touchy.html#a2f84e7a9eaf3a1907bca7d65f3f17fb2", null ],
    [ "y_cent", "classLAB__0X07_1_1touchy.html#ab78cb94afd219cb9ff27502a7c8f19ff", null ],
    [ "y_m", "classLAB__0X07_1_1touchy.html#a07c5b39e79b70781aae9e8ec41f7e092", null ],
    [ "y_p", "classLAB__0X07_1_1touchy.html#a27239a09b40589d828da2b6309cde8dc", null ],
    [ "y_tot", "classLAB__0X07_1_1touchy.html#adf4017ce300c755664610220083a003d", null ]
];