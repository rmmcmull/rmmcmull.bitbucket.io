/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Ryan McMullen Mechantronics Portfolio", "index.html", [
    [ "Portfolio Details", "index.html#sec_port", null ],
    [ "Lab 0X01 Documentation", "index.html#sec_lab1", null ],
    [ "Lab 0X02 Documentation*", "index.html#sec_lab2", null ],
    [ "Lab 0X03 Documentation*", "index.html#sec_lab3", null ],
    [ "Lab 0X04 Documentation*", "index.html#sec_lab4", null ],
    [ "Lab 0X05 Documentation", "index.html#sec_lab5", null ],
    [ "Lab 0X06 Documentation", "index.html#sec_lab6", null ],
    [ "Lab 0X07 Documentation*", "index.html#sec_lab7", null ],
    [ "Lab 0X08 Documentation*", "index.html#sec_lab8", null ],
    [ "Lab 0X09 Documentation*", "index.html#sec_lab9", null ],
    [ "Lab 5 Calculations", "Lab5_Hand_Calculations.html", null ],
    [ "Lab 6 Plots", "Lab6_Plots.html", null ],
    [ "Lab 9 Photos and Video", "Lab9.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"EncoderDriver_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';